.. _setup:

Setting up FastrPI
==================

Before FastrPI can be used, some things need to be set up. To do so run
.. codeblock::

  pip install fastrpi
  fastrpi init

After installing run ``fastrpi init`` and follow the instructions on the screen to configure your setup. In order
to use the public FastrPI repositories and follow the tutorial no credentials need to be set up.

For more information on the configuration of FastrPI, see :ref:`fastrpi_configuration` and more information
on the Fastr config file, can be found
`here <https://fastr.readthedocs.io/en/stable/static/file_description.html#config-file>`_.

Steps covered by ``fastrpi init``
=================================

While ``fastrpi init`` will guide you through setting up your FastrPI installation, below the steps are also
given for those that prefer this format.

Configure Fastr
---------------

Fastr needs to be configured to specify which of the Targets (DockerTarget or SingularityTarget) needs to be used
during the execution of the Networks. This can be configured in the
`config file <https://fastr.readthedocs.io/en/stable/static/file_description.html#config-file>`_, by default set to
``~/.fastr/config.py``, by setting
``preferred_target`` to

.. code-block:: python

    preferred_target = "DockerTarget" # For DockerTargets

for the use of Docker, or to use Singularity set

.. code-block:: python

    preferred_target = "SingularityTarget" # For SingularityTargets

The ``preferred_target`` needs to be compatible with the ``container_type`` setting in the FastrPI configuration file.

Git
---

FastrPI uses Git and GitLab as a backend, so Git (>=1.7.0) needs to be installed.
Your username and email should also be configured. If your name was "John Doe" and
your email was "johndoe@example.com", this could be configured by running

.. code-block::

    git config --global user.name "John Doe"
    git config --global user.email johndoe@example.com

Adjust these commands to reflect your own situation.
No further knowledge of Git is required to use FastrPI.

GitLab
------

The FastrPI repository itself is stored as a private GitLab repository.
To connect to this repository over SSH, you need to set up an SSH key.
The GitLab documentation gives instruction on how to
`generate an SSH key <https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair>`_ if necessary
and to `add this key to your GitLab account <https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account>`_.

To publish to the repository an Access Token is needed to read the API. To generate a token, see the
`GitLab documentation <https://docs.gitlab.com/ee/security/token_overview.html>`_.

