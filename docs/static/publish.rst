Creating, editing and publishing
================================

Within the FastrPI system Fastr Tools and Networks are distributed as packages.
The core information of the package, e.g. what type of package it is, what files
are in the package etc., is defined in the ``manifest.fastrpi_yaml`` file.

To add a package to the FastrPI repository it will need to be either created, or
an existing package needs to be edited, and it finally needs to be published.

Creating a new package
----------------------

To create a Tool package from a (collection of) Fastr Tool definition(s) run

.. code-block:: bash

    fastrpi create tool --name tool_name ./fastr_tool_1.yaml ./fastr_tool_2.yaml ...

To generate a Network package from a Fastr Network run

.. code-block:: bash

    fastrpi create network ./network.py

Editing an existing package
---------------------------

To edit an installed network one can run

.. code-block:: bash

    fastrpi edit network network_name -v version

By running this command the network files will be copied into a folder
in the directory from which you call this function. After editing the network
can be published under a different name or under a new version. Editing
an installed tool can be done in a similar fashion, by using

.. code-block:: bash

    fastrpi edit tool tool_name -v version -p package_version

Publishing a package
--------------------

When you are ready to send your network or tool package to FastrPI you can call:

.. code-block :: bash

    fastrpi publish network ./manifest.fastrpi_yaml

or

.. code-block :: bash

    fastrpi publish tool ./manifest.fastrpi_yaml

All manifests are in YAML format but in order to prevent Fastr from confusing the manifest files
with Tool files the extension is changed.



.. _package_checks:

Package checks
--------------


Before the package is uploaded a number of checks are done. First the manifest is checked
for validity of the YAML syntax. After that it is validated using a schema as
displayed below in the page on 'Manifests'. Below the checks for each type of package are specified.
Each of the checks will first be run locally and afterwards in a CI/CD pipeline.

Tool packages
^^^^^^^^^^^^^

For the Tool packages the checks are:

    - Check if the name is properly formatted. It cannot contain a dash, '-', for technical reasons.
    - Check if the name, version, package version combination is not taken.
    - Check if the required datatypes are in FastrPI.
    - For all tools under ``tools`` in the manifest:
        - Check if the YAML of the tool definition is valid YAML and a valid Fastr Tool.
        - Check if the command version and tool version in the tool definitions corresponds to the version and package version in the manifest respectively.
        - Check if the Fastr Tool contains a DockerTarget and a SingularityTarget, and both are formatted correctly.
        - Check if per tool definition a license is present. If not, a Creative Commons Attribution 4.0 license is proposed. This license is added if accepted.

Network packages
^^^^^^^^^^^^^^^^

For Network packages the checks are:

    - Check if the name is properly formatted. It cannot contain a dash, '-', for technical reasons.
    - Check if the name, package version combination is not taken.
    - Check if the required datatypes are in FastrPI.
    - Check if the required tool packages are in FastrPI.
    - Check if the network definition is a valid Fastr network.

Macronode packages
^^^^^^^^^^^^^^^^^^

For Macronode packages the checks are:

    - Check if the name is properly formatted. It cannot contain a dash, '-', for technical reasons. It also has to start with 'macro\_'.
    - Check if the name, package version combination is not taken.
    - Check if the corresponding Network is available in FastrPI.
    - For all tools under ``tools`` in the manifest:
        - Check if the YAML of the tool definition is valid YAML and a valid Fastr Tool.
        - Check if the tool version in the tool definitions corresponds to the package version in the manifest.
        - Check if the Fastr Tool contains the correct Target.
        - Check if per tool definition a license is present. If not, a Creative Commons Attribution 4.0 license is proposed. This license is added if accepted.


