.. _manifests:

Manifests
=========

Here are the schemas used for the manifest files. After each key the required type
is noted. Every item is required unless marked as optional. All filepaths must exist
and be relative paths to the manifest file.

Tool packages
-------------

.. code-block:: yaml

    name: str
    version: str
    package_version: str
    package_type: 'tool'
    dockerfile: str # Filepath
    license: str # Filepath
    required_datatypes: List[str]
    tools: # List
    - tool_definition: str # Filepath
      license: str # Filepath. Can be created with `fastrpi create`.
      ...
    files: # Optional. List
    - type: str # Only 'local' is supported
      path: str # Filepath
      ...

Network packages
----------------

.. code-block:: yaml

    name: str
    package_version: str
    package_type: 'network'
    network: str # Filepath
    license: str # Filepath
    required_datatypes: List[str]
    tool_packages: # List
    - name: str
      version: str
      package_version: str
      ...
    readme: str # Filepath
    cite: List[str]
    files: # Optional. List
    - type: str # Only 'local' is supported
      path: str # Filepath
      ...

Macronode packages
------------------

.. code-block:: yaml

    name: str # Must start with 'macro_'
    version: str
    package_version: str
    package_type: 'macronode'
    license: str # Filepath
    tools: # List
    - tool_definition: str # Filepath
      license: str # Optional. Filepath
      ...
    files: # Optional. List
    - type: str # Only 'local' is supported
      path: str # Filepath
      ...