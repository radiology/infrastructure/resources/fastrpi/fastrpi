Command-line interface reference
================================

.. click:: fastrpi.cli:cli
   :prog: fastrpi
   :nested: full


.. click:: fastrpi.server:cli
   :prog: fastrpi-server
   :nested: full

