******************
FastrPI Client
******************

.. include:: ../README.rst

FastrPI Client Documentation
============================

.. toctree::
    :maxdepth: 1

    static/setup.rst
    static/tutorial.rst
    static/configuration.rst
    static/manifests.rst
    static/publish.rst
    static/cli.rst
    fastrpi
    static/changelog.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
