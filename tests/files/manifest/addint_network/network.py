import fastr

def create_network():
    # Create initial network
    network = fastr.create_network(id='addint', version='2.0.0')

    # Create nodes
    source1 = network.create_source('Int', id='source1')
    source2 = network.create_source('Int', id='source2')
    sink1 = network.create_sink('Int', id='sink1')

    addint = network.create_node('addint/AddInt:1.0', tool_version='1.0',
                                id='addint')

    # Create links
    link1 = source1.output >> addint.inputs['left_hand']
    link2 = source2.output >> addint.inputs['right_hand']
    link3 = sink1.inputs['input'] << addint.outputs['result']
    
    return network
