import pytest

@pytest.mark.skip()
def test_container_init():
    pass

@pytest.mark.skip()
def test_container_not_available():
    pass

@pytest.mark.skip()
def test_container_ping():
    pass

@pytest.mark.skip()
def test_container_pull():
    pass

@pytest.mark.skip()
def test_docker_init():
    pass

@pytest.mark.skip()
def test_docker_not_available():
    pass

@pytest.mark.skip()
def test_docker_ping():
    pass

@pytest.mark.skip()
def test_docker_pull():
    pass

@pytest.mark.skip()
def test_singularity_init():
    pass

@pytest.mark.skip()
def test_singularity_not_available():
    pass

@pytest.mark.skip()
def test_singularity_ping():
    pass

@pytest.mark.skip()
def test_singularity_pull():
    pass
